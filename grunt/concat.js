module.exports = {
    lib: {
        files : {
            '<%= paths.dist %>/scripts/lib.js': [
                '<%= paths.bower %>/lodash/lodash.js',
                '<%= paths.bower %>/angular/angular.js',
                '<%= paths.bower %>/angular-route/angular-route.js',
                '<%= paths.bower %>/angular-touch/angular-touch.js',
                '<%= paths.bower %>/angular-animate/angular-animate.js',
            ]
        }
    },
    app: {
        files: {
            '<%= paths.dist %>/scripts/app.js': [
                '<%= paths.src %>/scripts/data.js',
                '<%= paths.src %>/scripts/**/*.js',
                '.tmp/templates.js',
                '<%= paths.src %>/scripts/app.js'
            ]
        }
    }
};