module.exports = {
	htaccess: {
		src: '<%= paths.src %>/.htaccess',
		dest: '<%= paths.dist %>/.htaccess',
	},
	svg: {
       files: [
            {
                expand: true,
                cwd: '<%= paths.src %>/svg',
                src: '*',
                dest: '<%= paths.dist %>/svg'
            }
        ]
    }
}