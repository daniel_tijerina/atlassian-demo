/**
* 
* track selection module
*/
angular.module('atlassian.app.directive.trackTabs', [])
.directive('trackTabs', [function(){
	// Runs during compile
	return {
		restrict: 'EA',
		scope: {
			model: '=ngModel',
			sessions: '='
		},
		replace: true,
		controller: 'trackTabsController',
		templateUrl: 'directives/track-tabs/track-tabs.tpl.html',
	};
}])
.controller('trackTabsController', ['$scope', '$interval', function($scope, $interval){

	$scope.tracks = [];


	/* Watch for data changes and collect the tracks */

	$scope.$watch('sessions', function (sessions) {
		$scope.tracks = [];
		for(o in sessions){
			if( $scope.tracks.indexOf(sessions[o].Track.Title) < 0 ){
				$scope.tracks.push(sessions[o].Track.Title);

				
			}
		}

		/* Sort the tracks by name */
		$scope.tracks.sort();

		if(!$scope.model && $scope.tracks){
			$scope.model = $scope.tracks[0];
		}
	});
	
	
}])