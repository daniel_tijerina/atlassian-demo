/**
* 
* track list module
*/
angular.module('atlassian.app.directive.trackList', [])
.directive('trackList', [function(){
	// Runs during compile
	return {
		restrict: 'EA',
		scope: {
			selectedTrack: '=ngModel',
			sessions: '=',
			selectedSession: '='
		},
		controller: 'trackListController',
		templateUrl: 'directives/track-list/track-list.tpl.html',
	};
}])
.controller('trackListController', ['$scope', '$interval', function($scope, $interval){

	/* Filter the sessions by the track */
	var _filterSessions = function () {
		if($scope.sessions && $scope.sessions.length && !$scope.selectedSession){
			$scope.selectedSession = $scope.sessions[0];
		}
	}

	$scope.$watch('sessions', _filterSessions);
	
}])