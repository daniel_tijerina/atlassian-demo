/**
* 
* track list module
*/
angular.module('atlassian.app.directive.sessionDetails', [])
.directive('sessionDetails', [function(){
	// Runs during compile
	return {
		restrict: 'EA',
		scope: {
			session: '=ngModel'
		},
		controller: 'sessionDetailsController',
		templateUrl: 'directives/session-details/session-details.tpl.html',
	};
}])
.controller('sessionDetailsController', ['$scope', function($scope){
	
}])