/**
*
* Video Data Service
*/
angular.module('atlassian.app.service.session', []).

factory('sessionService', [
	'$q',
	function( $q){
	
	return {

		getSessions: function () {
			var defer =  $q.defer();

			defer.resolve(window.sessions);

			return defer.promise;
		}
	};

}]);