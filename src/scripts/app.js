angular.module('atlassian.app', [
	'ngRoute',
	'atlassian.app.templates',
	'atlassian.app.service.session',
	'atlassian.app.directive.trackTabs',
	'atlassian.app.directive.trackList',
	'atlassian.app.directive.sessionDetails'
])

.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider.when('/:trackName?/:sessionId?', {
		templateUrl: 'views/home.tpl.html',
		controller: 'appController'
	});

	$locationProvider.html5Mode(true);
}])

.controller('appController', [
			'$scope', '$location', 'sessionService', '$routeParams', 
	function($scope,   $location,   sessionService, $routeParams){
	
	$scope.sessions = null;
	$scope.selectedTrack = $routeParams.trackName;
	$scope.selectedSessionId = $routeParams.sessionId;

	$scope.$watch('selectedTrack', function(){
		if($scope.selectedTrack){
			$scope.filteredSessions = _.filter($scope.sessions, {Track: {Title: $scope.selectedTrack}});
		}
	})


	sessionService.getSessions().then(function(data){
		$scope.sessions = data.Items;
		
		if($scope.selectedSessionId){
			$scope.selectedSession = _.findWhere($scope.sessions,{Id: $scope.selectedSessionId})
		}
	})
}])